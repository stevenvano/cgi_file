#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define DATAFILE "/var/www/html/fileS.json"

void unencode(char *src, char *last, char *dest)
{
    for (; src != last; src++, dest++)
        if (*src == '+')
        {
            *dest = ' ';
        }         
        else if (*src == '%')
        {
            int code;
            if (sscanf(src + 1, "%2x", &code) != 1)
            {
                code = '?';
            }
            else if(sscanf(src + 1, "%40", &code) != 1)
            {
                code = '@';
            }
            *dest = code;
            src += 2;
        }
        else
        {
            *dest = *src;
        }
    *++dest = '\0';
}

int main(void)
{
  //char *data
  char str1[15], str2[15], str3[30],str4[30], str5[15], str6[15], str7[64];
  char str31[12], str32[12];
  //Get local time in str7
  time_t t = time(NULL);
  struct tm *tm = localtime(&t);
  strftime(str7, sizeof(str7), "%c", tm);
  
  char *lenstr;
  char input[100], data[100];
  long len;
  char **tokens;
  char str[10000];
  
  
  printf("%s%c%c\n",
  "Content-Type:text/html;charset=iso-8859-1",13,10);
  printf("<TITLE>ToJsonSaver</TITLE>\n");
  printf("<H3>Saving Data to File</H3>\n");
  
  lenstr = getenv("CONTENT_LENGTH");
  if (lenstr == NULL || sscanf(lenstr, "%ld", &len) != 1)
    printf("<P>Error in invocation - wrong FORM probably.");
  else
  {
	  fgets(input,len+1, stdin);
	  unencode(input, input+len, data);
  }
 
   printf("data: %s",data);
 
  //data = getenv("QUERY_STRING");
  
  if(data!=NULL)
  {    
    sscanf(data,"firstName=%[0-9a-zA-Z]&lastName=%[0-9a-zA-Z]&Email=%[0-9A-Za-z-_@.]&Address=%[0-9A-Za-z- ]&City=%[0-9A-Za-z-]&Phone=%[0-9+/-]", str1, str2, str3, str4, str5, str6);
    //sscanf(str3,"%[0-9a-zA-Z.-_] %%40 %[0-9A-Za-z-_.]",str31,str32);
    printf("First Name: %s<BR>", str1);
    printf("Last Name: %s<BR>", str2);
    printf("Email: %s<BR>", str3);
    //printf("String31: %s<BR>", str31);
    //printf("Email:  %s@%s<BR>", str31,str32);
    printf("Address: %s<BR>", str4);
    printf("City: %s<BR>", str5);
    printf("Phone: %s<BR>", str6);
    printf ("Date: %s<BR>", str7);
    printf("<BR>");
  
  
    FILE * fp;
    int c;
    int i =0;
    char x[12000];
    char *ch;
    fp = fopen(DATAFILE,"r");
    if (fp == NULL)
    {
      printf("file not found<BR>");
    }
    else
    {
      printf("Opened json file<BR>");
      while(1) {
        c =fgetc(fp);
        if( feof(fp)){
            break ;
        }          
        if(c == ']'){
          break;
          x[i] = ' ';
          i++;
        }
        x[i]= c;
        i++;
      }
      printf("%s",x);
      printf("<BR>Length: %ld",i);
      fclose(fp);
      printf("<BR>closed readed file<BR><BR>");
    }
    
    fp = fopen(DATAFILE,"w+");
    if (fp == NULL)
    {
      printf("file not found<BR>");
    }
    else
    {  
      printf("%c",x[i]);
      printf("opened json file<BR>");
      if(x[0] != '[')
      {
         fprintf(fp, "[{\"First\":\"%s\",\"Last Name\":\"%s\",\"Email\":\"%s\",\"Address\":\"%s\",\"City\":\"%s\",\"Phone\":\"%s\",\"Date\":\"%s\"}]",str1,str2,str3,str4,str5,str6,str7);
      }
      else
      {
         fprintf(fp, "%s,{\"First\":\"%s\",\"Last Name\":\"%s\",\"Email\":\"%s\",\"Address\":\"%s\",\"City\":\"%s\",\"Phone\":\"%s\",\"Date\":\"%s\"}]",x,str1,str2,str3,str4,str5,str6,str7);
      }
      printf("Data : [{\"First Name\":\"%s\",\"Last Name\":\"%s\",\"Email\":\"%s\",\"Address\":\"%s\",\"City\":\"%s\",\"Phone\":\"%s\",\"Date\":\"%s\"}]<BR>",str1,str2,str3,str4,str5,str6,str7);

    
      fclose(fp);
      printf("closed file<BR>");
    }
    
  }
  else
  printf("<H1>Error passing data to CGI Script</H1>");

  // Redirect to your previous page
  const char *redirect_page_format =
  "<html>\n"
  "<head>\n"
  "<meta http-equiv=\"REFRESH\"\n"
  "content=\"0;url=%s\">\n"
  "</head>\n"
  "</html>\n";
  printf(redirect_page_format, getenv("HTTP_REFERER"));

  return 0;
}


